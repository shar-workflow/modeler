package main

import (
	"fmt"
	"gitlab.com/shar-workflow/modeler/lib/validator"
)

func main() {
	v, err := validator.VarsFromExpr("y + b + 91")
	fmt.Println(v, err)
}
