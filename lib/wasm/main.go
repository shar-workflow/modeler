package main

import (
	"fmt"
	"github.com/antonmedv/expr/file"
	"gitlab.com/shar-workflow/modeler/lib/validator"
	"syscall/js"
)

func main() {
	fmt.Println("Go Web Assembly")
	js.Global().Set("compileExpr", compileWrapper())
	js.Global().Set("varsFromExpr", varsFromExprWrapper())
	select {}
}

func compileWrapper() js.Func {
	jsonFunc := js.FuncOf(func(this js.Value, args []js.Value) any {
		if len(args) != 1 {
			return "Invalid no of arguments passed"
		}
		inputJSON := args[0].String()
		fmt.Printf("input %s\n", inputJSON)

		err := validator.Compile(inputJSON)
		if err != nil {
			fe := err.(*file.Error)
			return map[string]any{
				"message": fe.Message,
				"line":    fe.Line,
				"column":  fe.Column,
				"snippet": fe.Snippet,
			}
		}
		return nil
	})
	return jsonFunc
}

func varsFromExprWrapper() js.Func {
	jsonFunc := js.FuncOf(func(this js.Value, args []js.Value) any {
		if len(args) != 1 {
			return "Invalid no of arguments passed"
		}
		inputJSON := args[0].String()
		fmt.Printf("input %s\n", inputJSON)
		vars, err := validator.VarsFromExpr(inputJSON)
		if err != nil {
			return err.Error()
		}
		ret := make(map[string]interface{}, len(vars))
		for i := range vars {
			ret[i] = i
		}
		return ret
	})
	return jsonFunc
}
