package validator

import (
	"fmt"
	"github.com/antonmedv/expr"
	"github.com/antonmedv/expr/ast"
	"github.com/antonmedv/expr/parser"
	"strings"
)

// Compile compiles an expression and returns errors
func Compile(exp string) error {
	if len(exp) == 0 {
		return nil
	}
	if exp[0] == '=' {
		exp = exp[1:]
	}
	if len(exp) == 0 {
		return nil
	}
	vm, err := expr.Compile(exp)
	fmt.Println(vm)
	return err
}

// VarsFromExpr returns a list of variables mentioned in an expression
func VarsFromExpr(exp string) (map[string]struct{}, error) {
	ret := make(map[string]struct{})
	exp = strings.TrimSpace(exp)
	if len(exp) == 0 {
		return ret, nil
	}
	if exp[0] == '=' {
		exp = exp[1:]
	}
	if len(exp) == 0 {
		return ret, nil
	}
	c, err := parser.Parse(exp)
	if err != nil {
		return nil, fmt.Errorf("get variables failed to parse expression %w", err)
	}

	g := &variableWalker{v: make(map[string]struct{})}
	ast.Walk(&c.Node, g)
	return g.v, nil
}

type variableWalker struct {
	v map[string]struct{}
}

func (w *variableWalker) Visit(n *ast.Node) {
	switch t := (*n).(type) {
	case *ast.IdentifierNode:
		w.v[t.Value] = struct{}{}
	}
}

// Enter is called from the visitor to iterate all IdentifierNode types
func (w *variableWalker) Enter(n *ast.Node) {

}

// Exit is unused in the variableWalker implementation
func (w *variableWalker) Exit(_ *ast.Node) {}
