default: build

build:wasm .FORCE
	cd modeler && npm install --save-dev @electron-forge/cli
	cd modeler && npm install && npm run make
	cd modeler && mv out/make/* ../build/
wasm:.FORCE
	cd lib && GOOS=js GOARCH=wasm go build -o ../build/wasm/go-lib.wasm wasm/main.go && cp ../build/wasm/go-lib.wasm web/www/
.FORCE: