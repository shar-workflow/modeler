const { writeFileSync } = require("fs");
const { dialog } = require('electron');

// saves a file using the native file saver dialog
module.exports.dialogFileSave = function saveFileUsingTheDialog(mainWindow, fileContents) {
  const selectedPath = dialog.showSaveDialogSync(mainWindow, {
    filters: [{ name: "BPMN", extensions: ['bpmn'] }] 
  });
  if (!selectedPath) {
    return;
  }
  
  writeFileSync(selectedPath, fileContents);
  return selectedPath;
}

module.exports.dialogSaveBeforeClosing = function saveFileBeforeClosingDialog(mainWindow) {
  return dialog.showMessageBoxSync(mainWindow, {
    title: "Confirm",
    message: "Save changes to current tab before closing?",
    type: "question",
    buttons: ["Save", "Don't Save", "Cancel"],
  });
}
