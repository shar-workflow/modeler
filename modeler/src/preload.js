// See the Electron documentation for details on how to use preload scripts:
// https://www.electronjs.org/docs/latest/tutorial/process-model#preload-scripts
const { contextBridge, ipcRenderer } = require("electron");
const { IPCChannel } = require("./ipc");

contextBridge.exposeInMainWorld(
  "api", 
  {
    send: (channel, data) => {
      const validChannels = [IPCChannel.TO_MAIN];
      if (validChannels.includes(channel)) {
        ipcRenderer.send(channel, data);
      }
    },
    receive: (channel, func) => {
      const validChannels = [IPCChannel.FROM_MAIN];
      if (validChannels.includes(channel)) {
        ipcRenderer.on(channel, (event, ...args) => func(args));
      }
    }
  }
);
