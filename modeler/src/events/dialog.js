export const FileSaveDialogChoice = Object.freeze({
  SAVE: 0,
  DONT_SAVE: 1,
  CANCEL: 2,
});
