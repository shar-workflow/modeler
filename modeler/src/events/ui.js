export const EventsUI = {
  // whenever the modeler changes
  ModelerChange: 'ui:modeler:change',
  TabsChange: 'ui:tabs:change',
  NoTab: 'ui:tabs:no-tab',
  CloseTab: 'ui:tabs:close-tab',
  WorkflowNew: 'ui:workflow:new',
  WorkflowLoad: 'ui:workflow:load',
};
