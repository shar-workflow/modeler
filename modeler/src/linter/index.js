import { Linter } from 'bpmnlint';
import linterConfig from './.bpmnlintrc';

const linter = new Linter(linterConfig);

export default linter;
