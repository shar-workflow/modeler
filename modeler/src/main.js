const { app, BrowserWindow, Menu, ipcMain } = require('electron');
const { IPCChannel, IPCOperationType } = require('./ipc');
const { buildAppMenu } = require("./menu"); 
const { writeFileSync } = require("fs");
const { dialogFileSave, dialogSaveBeforeClosing } = require('./fs');

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) {
  app.quit();
}

let mainWindow;
const createWindow = () => {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    title: "SHAR Modeler",
    width: 1024,
    height: 768,
    minWidth: 1024,
    minHeight: 768,
    webPreferences: {
      preload: MAIN_WINDOW_PRELOAD_WEBPACK_ENTRY,
      devTools: true,
      webviewTag: true,
      nodeIntegration: true,
    },
  });

  // load the menu;
  Menu.setApplicationMenu(buildAppMenu(mainWindow));

  // and load the index.html of the app.
  mainWindow.loadURL(MAIN_WINDOW_WEBPACK_ENTRY);
  mainWindow.webContents.openDevTools();
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

// listen to IPC events
ipcMain.on(IPCChannel.TO_MAIN, (_, message) => {
  console.log("received message type:", message.type);
  switch(message.type) {
    case IPCOperationType.SAVE_FILE: {}
      let path = "";
      if (message.payload.filePath) {
        writeFileSync(message.payload.filePath, message.payload.content);
        path = message.payload.filePath;
      } else {
        // if new file show the dialog save
        path = dialogFileSave(mainWindow, message.payload.content);
      }
      mainWindow.webContents.send(IPCChannel.FROM_MAIN, { type: IPCOperationType.FILE_SAVED, payload: { path } });
      break;
    case IPCOperationType.SAVE_FILE_AS:
      const filePath = dialogFileSave(mainWindow, message.payload.content);
      mainWindow.webContents.send(IPCChannel.FROM_MAIN, { type: IPCOperationType.FILE_SAVED, payload: { path: filePath} });
      break;
    case IPCOperationType.CLOSE_TAB:
      const choice = dialogSaveBeforeClosing(mainWindow);
      mainWindow.webContents.send(IPCChannel.FROM_MAIN, { type: IPCOperationType.CLOSE_TAB, payload: { choice, tabID: message.payload.tabID }});
      break;
    case IPCOperationType.MENU_NEW_FILE:
      Menu.getApplicationMenu().getMenuItemById('new-file').click();
      break;
    case IPCOperationType.MENU_LOAD_FILE:
      Menu.getApplicationMenu().getMenuItemById('open-file').click();
      break;
    default:
      console.log("invalid message");
      break;
  }
});
