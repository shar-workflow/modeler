const IPCOperationType = Object.freeze({
  NEW_FILE: 1,
  OPEN_FILE: 2,
  SAVE_FILE: 3,
  SAVE_FILE_AS: 4,
  FILE_SAVED: 5,
  CLOSE_TAB: 6,
  MENU_NEW_FILE: 7,
  MENU_LOAD_FILE: 8,
});

const IPCChannel = Object.freeze({
  TO_MAIN: "toMain",
  FROM_MAIN: "fromMain",
});

module.exports = {
  IPCOperationType,
  IPCChannel,
}
