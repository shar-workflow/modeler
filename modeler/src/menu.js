const { app, Menu, dialog } = require('electron');
const { readFileSync } = require('fs');
const { IPCOperationType, IPCChannel } = require('./ipc');

const isMac = process.platform === 'darwin';

export function buildAppMenu(mainWindow) {
  return Menu.buildFromTemplate([
    // app menu on MAC
    ...(isMac ? [{
      label: app.name,
      submenu: [
        { role: 'about' },
        { type: 'separator' },
        { role: 'services' },
        { type: 'separator' },
        { role: 'hide' },
        { role: 'hideOthers' },
        { role: 'unhide' },
        { type: 'separator' },
        { role: 'quit' }
      ]
    }] : []),
    // file menu
    {
      label: 'File',
      submenu: [
        {
          id: 'new-file',
          label: 'New File',
          accelerator: 'CmdOrCtrl+N',
          click() {
            mainWindow.webContents.send(IPCChannel.FROM_MAIN, {
              type: IPCOperationType.NEW_FILE,
            });
          }
        },
        {
          id: 'open-file',
          label:'Open File',
          accelerator: 'CmdOrCtrl+O',
          // this is the main bit hijack the click event 
          async click() {
            try {
              const file = await openFileHandler();
              if (file) {
                mainWindow.webContents.send(IPCChannel.FROM_MAIN, {
                  type: IPCOperationType.OPEN_FILE,
                  payload: file,
                });
              }
            } catch(err) {
              console.log(err);
            }
          } 
        },
        { type: "separator" },
        {
          label: 'Save File',
          accelerator: 'CmdOrCtrl+S',
          click: function() {
            mainWindow.webContents.send(IPCChannel.FROM_MAIN, {
              type: IPCOperationType.SAVE_FILE,
            });
          }
        },
        {
          label: 'Save File As..',
          accelerator: 'CmdOrCtrl+Shift+S',
          click: function() {
            mainWindow.webContents.send(IPCChannel.FROM_MAIN, {
              type: IPCOperationType.SAVE_FILE_AS,
            });
          }
        },
        { type: "separator" },
        isMac ? { role: 'close' } : { role: 'quit' },
      ]
    },
    // edit menu
    {
      label: 'Edit',
      submenu: [
        { role: 'undo' },
        { role: 'redo' },
        { type: 'separator' },
        { role: 'cut' },
        { role: 'copy' },
        { role: 'paste' },
        ...(isMac ? [
          { role: 'pasteAndMatchStyle' },
          { role: 'delete' },
          { role: 'selectAll' },
          { type: 'separator' },
          {
            label: 'Speech',
            submenu: [
              { role: 'startSpeaking' },
              { role: 'stopSpeaking' }
            ]
          }
        ] : [
          { role: 'delete' },
          { type: 'separator' },
          { role: 'selectAll' }
        ])
      ]
    },
    // view menu
    {
      label: 'View',
      submenu: [
        { role: 'reload' },
        { role: 'forceReload' },
        { role: 'toggleDevTools' },
        { type: 'separator' },
        { role: 'resetZoom' },
        { role: 'zoomIn' },
        { role: 'zoomOut' },
        { type: 'separator' },
        { role: 'togglefullscreen' }
      ]
    },
    // window menu
    {
      label: 'Window',
      submenu: [
        { role: 'minimize' },
        { role: 'zoom' },
        ...(isMac ? [
          { type: 'separator' },
          { role: 'front' },
          { type: 'separator' },
          { role: 'window' }
        ] : [
          { role: 'close' }
        ])
      ]
    },
  ]);   
}

async function openFileHandler() {
  try {
    const fileObj = await dialog.showOpenDialog({
      filters: [{ name: "BPMN files", extensions: ['bpmn']}],
      properties: ['openFile'],
    });

    if (!fileObj.canceled) {
      // read file
      const content = readFileSync(fileObj.filePaths[0], { encoding: "utf-8" });
      return {
        path: fileObj.filePaths[0],
        content,
      }
    }
  } catch(err) {
    console.log(err);
    throw err;
  }
}