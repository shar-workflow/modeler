import { IPCOperationType, IPCChannel } from './ipc';
import Storage from './lib/storage';
import { TabManager } from './ui/tab-manager';
import { WelcomeUI } from './ui/welcome';
import BPMNModeler from './ui/bpmn-modeler';
import emptyBPMN from './assets/empty.bpmn';
import EventEmitter from 'events';
import { EventsUI, FileSaveDialogChoice } from './events';

// shared bus used to handle events between UI components
const eventBus = new EventEmitter();

// bpmnModeler handles interaction with the bpmnjs lib
const bpmnModeler = new BPMNModeler(
  { 
    container: "modeler",
    modeler: "js-canvas",
    propertiesPanel: "js-properties-panel"
  },
  eventBus,
);

// tab manager handles interactions with tabs
const tabManager = new TabManager("tab-manager", eventBus);
tabManager.listenForEvents();

// welcome screen handles interactions with the welcome screen
const welcomeScreen = new WelcomeUI("welcome-container", eventBus);
welcomeScreen.listenForEvents();

// opens the xml (bpmn workflow) in the modeler
// note: xml should be valid BPMN XML
async function openWorkflow(xml) {
  try {
    welcomeScreen.hide();
    bpmnModeler.show();
    await bpmnModeler.openWorkflow(xml);
  } catch (err) {
    console.log("Couldn't open workflow: ", err);
  }
}

// handler to create an empty workflow in the modeler
async function createNewWorkflowHandler() {
  openWorkflow(emptyBPMN);
  tabManager.addNewTab();
}

// handler to open an existing workflow in the modeler
function openWorkflowFromFileHandler(file) {
  welcomeScreen.hide();
  bpmnModeler.show();
  const fileName = file.path.replace(/^.*[\\\/]/, '')
  const tabID = tabManager.addNewTab(fileName);
  // TODO: this is a workaround. The addNewTab will trigger an event that will check if the new tab 
  // has any content stored locally (which in this case will not since its a new tab opened from Open File menu)
  // in this case the event handler will open an empty workflow
  // we override that by opening the diagram based on the file content
  setTimeout(() => {
    // save the path on the storage
    const tabState = Storage.getItem(`tab_${tabID}`) || {};
    tabState.path = file.path;
    Storage.setItem(`tab_${tabID}`, tabState);
    openWorkflow(file.content);
  }, 250);
}

// handler to save the changes made to the active workflow
async function saveFileHandler(tabID) {
  try {
    const { xml } = await bpmnModeler.saveWorkflow();
    // we asume everything its there
    const activeTab = tabID || Storage.getItem('currentActiveTab');
    const activeTabState = Storage.getItem(`tab_${activeTab}`);
    sendToMain(IPCOperationType.SAVE_FILE, {
      filePath: activeTabState.path,
      content: xml,
    });
  } catch (err) {
    console.log("Couldn't save BPMN: ", err);
  }
}

// handler to save the changes made to the active workflow in a different file
async function saveFileAsHandler() {
  try {
    const { xml } = await bpmnModeler.saveWorkflow();
    sendToMain(IPCOperationType.SAVE_FILE_AS, { content: xml });
  } catch (err) {
    console.log("Couldn't save BPMN: ", err);
  }
}

// handler to react to file saved event on the active workflow
function fileSavedHandler({ path }) {
  const activeTabID = Storage.getItem('currentActiveTab');
  const tabKey = `tab_${activeTabID}`;
  const activeTabState = Storage.getItem(tabKey);
  // this shouldn't be the case in 99.999% of the cases
  if (!activeTabState) {
    console.warn("fileSavedHandler: cannot store saved file path as there is no active tab state");
    return;
  }

  activeTabState.path = path;
  // remove dirty from tab since it was already saved
  activeTabState.dirty = false;
  tabManager.toggleTabDirty(false);
  Storage.setItem(tabKey, activeTabState);
}

// handler to handle close tab events
async function closeTabHandler({ choice, tabID }) {
  switch (choice) {
    // save
    case FileSaveDialogChoice.SAVE:
      // present the save dialog first
      const tabKey = `tab_${tabID}`;
      const state = Storage.getItem(tabKey) || {};
      if (state.path) {
        await saveFileHandler(tabID);
      } else {
        await saveFileAsHandler();
      }
      tabManager.doCloseTab(tabID);
      break;
    // don't save
    case FileSaveDialogChoice.DONT_SAVE:
      tabManager.doCloseTab(tabID);
      break;
    // cancel, do not close the tab  
    case FileSaveDialogChoice.CANCEL:
      break;
    default:
      // nothing
      break;
  }
}

// saves the workflow state for the corresponding tab in storage
async function saveWorkflowStateForTab(tabID) {
  try {
    const { xml } = await bpmnModeler.saveWorkflow();
    const tabKey = `tab_${tabID}`;
    const state = Storage.getItem(tabKey) || {};
    // change only the content of the store for this tab
    state.content = xml;
    Storage.setItem(tabKey, state);
  } catch (err) {
    console.error("cannot store current workflow state", err);
  }
}

// opens the workflow associated with the tab and loads it in the modeler
// if there isn't any workflow state associated with the tab, an empty workflow will be loaded
async function loadWorkflowStateForTab(tabID) {
  try {
    const tabKey = `tab_${tabID}`;
    const state = Storage.getItem(tabKey) || {};
    if (state.content) {
      openWorkflow(state.content);
      return;
    }

    openWorkflow(emptyBPMN);
  } catch (err) {
    console.error("cannot store current workflow state", err);
  }
}

// marks the currently selected tab as dirty
// the dirty flag is used to trigger file save dialog if the user tries to close tab
// before saving the changes
function markCurrentTabAsDirty() {
  const activeTabID = Storage.getItem('currentActiveTab');
  if (!activeTabID) {
    console.warn("markCurrentTabAsDirty: cannot mark tab as dirty as there is no active tab");
    return;
  }

  const tabKey = `tab_${activeTabID}`;
  const activeTabState = Storage.getItem(tabKey) || {};
  activeTabState.dirty = true;

  tabManager.toggleTabDirty(true);
  Storage.setItem(tabKey, activeTabState);
}

/**
 *  HANDLE IPC FROM MAIN PROCESS
 * 
 */
window.api.receive(IPCChannel.FROM_MAIN, (event) => {
  // event is an array
  const message = event[0];
  switch(message.type) {
    case IPCOperationType.NEW_FILE:
      createNewWorkflowHandler();
      break;
    case IPCOperationType.OPEN_FILE:
      openWorkflowFromFileHandler(message.payload);
      break;
    case IPCOperationType.SAVE_FILE:
      saveFileHandler();
      break;
    case IPCOperationType.SAVE_FILE_AS:
      saveFileAsHandler();
      break;
    case IPCOperationType.FILE_SAVED:
      fileSavedHandler(message.payload);
      break;
    case IPCOperationType.CLOSE_TAB:
      closeTabHandler(message.payload);
      break;  
    default:
      console.log("invalid IPC operation", message);
      break;
  }
});

// sends an event to the main process with the provided type and payload
function sendToMain(type, payload) {
  window.api.send(IPCChannel.TO_MAIN, { type, payload });
}

/**
 *  HANDLE UI EVENTS
 * 
 */

// MODELER EVENTS
// hook into bpmn changes in order to mark tabs as dirty
eventBus.on(EventsUI.ModelerChange, () => {
  markCurrentTabAsDirty();
});

// TAB EVENTS
/* 
  Cases to handle on tab change
  1. currentTabID != null
    - Save the currentTabID modeler state in the local storage, while preserving any previous state saved for that tab
    - Try and load the saved state for the newTabID or create an empty workflow
  2. currentTabID == null
    - Try and load the saved state for the newTabID or create an empty workflow
*/
eventBus.on(EventsUI.TabsChange, async ({ currentTabID, newTabID }) => {
  if (currentTabID) {
    await saveWorkflowStateForTab(currentTabID);
  }

  await loadWorkflowStateForTab(newTabID);
  Storage.setItem("currentActiveTab", newTabID);
});

/* 
  Cases to handle on tab close
  1. current tab is dirty
    - Present the save file dialog
    - check possibility of cleaning up the tab state
    - close tab
  2. current tab is not dirty
    - close tab
    - clean any tab state
  
  After all of them are done, mark a new tab as active
*/
eventBus.on(EventsUI.CloseTab, ({ tabID }) => {
  const tabState = Storage.getItem(`tab_${tabID}`);
  // no need to do anything, workflow is empty
  // just clean the storage
  if (!tabState || !tabState.dirty) {
    Storage.removeItem(`tab_${tabID}`);
    tabManager.doCloseTab(tabID);
    return;
  }

  // present dialog before closing
  sendToMain(IPCOperationType.CLOSE_TAB, { tabID });
});

// show welcome screen when there are not tabs
eventBus.on(EventsUI.NoTab, () => {
  welcomeScreen.show();
  bpmnModeler.hide();
});

// WELCOME SCREEN EVENTS
// create a new workflow
eventBus.on(EventsUI.WorkflowNew, () => {
  sendToMain(IPCOperationType.MENU_NEW_FILE);
});

// load existing workflow
eventBus.on(EventsUI.WorkflowLoad, () => {
  sendToMain(IPCOperationType.MENU_LOAD_FILE);
});
