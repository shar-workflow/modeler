import IDs from 'ids';

const idsGenerator = new IDs([36, 36, 1]);

// generates a random id using the ids package
export function generateID() {
  return idsGenerator.next();
}
