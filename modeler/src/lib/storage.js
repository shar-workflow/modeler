class Storage {
  setItem(key, value) {
    localStorage.setItem(key, JSON.stringify(value));
  }

  getItem(key) {
    const item = localStorage.getItem(key);
    if (item) {
      return JSON.parse(item);
    }

    return null;
  }

  removeItem(key) {
    return localStorage.removeItem(key);
  }
}

export default new Storage();