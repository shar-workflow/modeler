import { EventsUI } from "../events";
import { generateID } from "../util/id";

export class TabManager {
  constructor(elementID, emitter) {
    this.container = document.getElementById(elementID);
    this.emitter = emitter;
    this.counter = 1;
  }

  // listens for click event on the tab container
  listenForEvents() {
    this.container.onclick = (ev) => {
      const el = ev.target;
      const type = el.getAttribute('data-type');
      if (type === "add-tab") {
        this.addNewTab();
      }
    }
  }

  // adds a new tab in the UI triggered from the add button
  addNewTab(name) {
    const tab = document.createElement("div");
    // set a random tab-id
    const tabID = generateID()
    tab.setAttribute("data-id", tabID);
    tab.className = "tab-item";
    tab.onclick = () => { this.makeTabActive(tab); };
    // append tab title
    tab.append(name || "untitled workflow " + this.counter++);
    // append close element
    const closeEl = document.createElement('p');
    closeEl.innerHTML = "x";
    closeEl.className = "close";
    closeEl.onclick = (ev) => { 
      ev.stopPropagation();
      this.closeTabEvent(closeEl);
    };
    tab.append(closeEl);
    // insert the new tab before the + button
    this.container.insertBefore(tab, this.container.children[this.container.children.length - 1]);
    this.makeTabActive(tab);
    return tabID;
  }

  // makeTabActive activates the selected tab
  makeTabActive(el) {
    // check if tab is already active
    if (el.classList.contains("active")) {
      return;
    }

    // remove the current active tab and save its state locally
    let currentTabID = null;
    const activeTab = document.getElementsByClassName("tab-item active")[0];
    if (activeTab) {
      currentTabID = activeTab.getAttribute("data-id");
      activeTab.classList.remove('active');
    }

    // make new tabe active
    el.classList.add("active");
    this.emitter.emit(EventsUI.TabsChange, {
      currentTabID,
      newTabID: el.getAttribute('data-id'),
    });
  }

  // emits a closeTab intent to interested listeners
  closeTabEvent(element) {
    const tab = element.parentElement;
    this.emitter.emit(EventsUI.CloseTab, { tabID: tab.getAttribute('data-id') });
  }

  // removes tab from the UI
  doCloseTab(tabID) {
    const tab = document.querySelector(`[data-id="${tabID}"]`);
    // no tab found with the passed tabID
    if (!tab) {
      return;
    }

    // check if the tab we want to close tab is active
    // if not active we can proceed and close it
    const isActive = tab.classList.contains('active');
    if (!isActive) {
      tab.remove();
      return;
    }

    // try and pick another tab to make it active
    let sibling = tab.nextElementSibling;
    // if the next sibling is the add button try to check for previous siblings
    if (sibling.getAttribute('data-type') === 'add-tab') {
      // there is no previous sibling as well
      const prevSibling = tab.previousElementSibling;
      if (prevSibling === null) {
        // show welcome screen
        tab.remove();
        this.emitter.emit(EventsUI.NoTab);
        return;
      }

      sibling = prevSibling;
    }

    tab.remove();
    this.makeTabActive(sibling);
  }

  // adds UI that marks tab dirty
  toggleTabDirty(dirty) {
    const activeTab = document.getElementsByClassName("tab-item active")[0];
    if (!activeTab) {
      return;
    }

    dirty ? activeTab.classList.add('dirty') : activeTab.classList.remove('dirty');
  }
}
