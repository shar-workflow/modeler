import BpmnModeler from 'bpmn-js/lib/Modeler';
import {
  BpmnPropertiesPanelModule,
  BpmnPropertiesProviderModule,
  ZeebePropertiesProviderModule
} from 'bpmn-js-properties-panel';
import ZeebeBpmnModdle from 'zeebe-bpmn-moddle/resources/zeebe.json';
import lintModule from 'bpmn-js-bpmnlint';
import bpmnLinter from '../linter';
import { EventsUI } from '../events';

// styles
import 'bpmn-js/dist/assets/bpmn-js.css';
import 'bpmn-js/dist/assets/diagram-js.css';
import 'bpmn-js/dist/assets/bpmn-font/css/bpmn-embedded.css';
import 'bpmn-js-properties-panel/dist/assets/properties-panel.css';
import 'bpmn-js-bpmnlint/dist/assets/css/bpmn-js-bpmnlint.css';
import '../index.css';

export default class BPMNModeler {
  constructor(uiIDs, emitter) {
    this.emitter = emitter;
    this.container = document.getElementById(uiIDs["container"]);

    this.modeler = new BpmnModeler({
      container: `#${uiIDs["modeler"]}`,
      propertiesPanel: {
        parent: `#${uiIDs["propertiesPanel"]}`
      },
      keyboard: {
        bindTo: window,
      },
      additionalModules: [
        BpmnPropertiesPanelModule,
        BpmnPropertiesProviderModule,
        ZeebePropertiesProviderModule,
        lintModule,
      ],
      moddleExtensions: {
        zeebe: ZeebeBpmnModdle
      },
      linting: {
        bpmnlint: bpmnLinter
      }
    });

    this.modeler.on("import.done", this._onImportDone.bind(this));
    this.modeler.on("commandStack.changed", this._onChange.bind(this));
  }

  _onImportDone(event) {
    if (event.error) {
      console.log("error while loading workflow:", event.error);
      return;
    }
  
    // fit the workflow inside the canvas
    this.modeler.get('canvas').zoom('fit-viewport');
    // turn on the linter as its off by default
    this.modeler.get('linting').toggle(true);
  }

  _onChange(_args) {
    this.emitter.emit(EventsUI.ModelerChange);
  }

  openWorkflow(xml) {
    return this.modeler.importXML(xml);
  }

  saveWorkflow() {
    return this.modeler.saveXML({ format: true });
  }

  show() {
    this.container.style.display = "flex";
  }

  hide() {
    this.container.style.display = "none";
  }
}