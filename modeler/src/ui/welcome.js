import { EventsUI } from "../events";

export class WelcomeUI {
  constructor(elementID, emitter) {
    this.container = document.getElementById(elementID);
    this.emitter = emitter;

    this.options = {
      NEW_WORKFLOW: 'new-workflow',
      LOAD_WORKFLOW: 'load-workflow',
    };
  }

  // listens for click event on the welcome container
  listenForEvents() {
    this.container.onclick = (ev) => {
      const el = ev.target;
      if (el.id === this.options.NEW_WORKFLOW) {
        this.emitter.emit(EventsUI.WorkflowNew);
        return;
      }

      if (el.id === this.options.LOAD_WORKFLOW) {
        this.emitter.emit(EventsUI.WorkflowLoad);
        return;
      }
    }
  }

  hide() {
    this.container.style.display = "none";
  }

  show() {
    this.container.style.display = "flex";
  }
}
