const rules = require('./webpack.rules');

rules.push({
  test: /\.css$/,
  use: [{ loader: 'style-loader' }, { loader: 'css-loader' }],
});

rules.push({
  test: /\.bpmn$/,
  type: 'asset/source'
});

module.exports = {
  // Put your normal webpack config below here
  module: {
    rules,
  },
};
